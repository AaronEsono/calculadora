package org.villablanca;

public class Calculadora {
	
	public static int producto(int a, int b) {
		return a*b;
	}

	public static int suma(int x, int y) {
		return x + y;
	}
	
	public static void main(String[] args) {		
		System.out.println("Operacion: ");
		
		System.out.println(producto(3, 5));

	}

}
